Antoine de Saint-Exupery: Mały Książe

LEONOWI WERTH
Przepraszam wszystkie dzieci za poświęcenie tej ksiąŜki dorosłemu. Mam waŜne ku temu powody: ten
dorosły jest moim najlepszym przyjacielem na świecie. Drugi powód: ten dorosły potrafi zrozumieć
wszystko, nawet ksiąŜki dla dzieci. Mam teŜ trzeci powód: ten dorosły znajduje się we Francji, gdzie cierpi
głód i chłód. I trzeba go pocieszyć. Jeśli te powody nie wystarczą - chętnie poświęcę tę ksiąŜkę dziecku,
jakim był kiedyś ten dorosły. Wszyscy dorośli byli kiedyś dziećmi. Choć niewielu z nich o tym pamięta.
Zmieniam więc moją dedykację: LEONOWI WERTH, gdy był małym chłopcem

I
Gdy miałem sześć lat, zobaczyłem pewnego razu wspaniały obrazek w ksiąŜce opisującej puszczę dziewiczą.
KsiąŜka nazywała się "Historie prawdziwe". Obrazek przedstawiał węŜa boa, połykającego drapieŜne
zwierzę. Oto kopia rysunku:
W ksiąŜce było napisane: "WęŜe boa połykają w całości schwytane zwierzęta. Następnie nie mogą się ruszać
i śpią przez sześć miesięcy, dopóki zdobycz nie zostanie strawiona".
Po obejrzeniu obrazka wiele myślałem o Ŝyciu dŜungli. Pod wpływem tych myśli udało mi się przy pomocy
kredki stworzyć mój pierwszy rysunek. Rysunek numer 1. Wyglądał on tak:
(...)

II
W ten sposób, nie znajdując z nikim wspólnego języka, prowadziłem samotne Ŝycie aŜ do momentu
przymusowego lądowania na Saharze. Było to sześć lat temu. Coś się zepsuło w motorze. PoniewaŜ nie
towarzyszył mi ani mechanik, ani pasaŜerowie, musiałem sam zabrać się do bardzo trudnej naprawy. Była to
dla mnie kwestia Ŝycia lub śmierci. Miałem zapas wody zaledwie na osiem dni.
Pierwszego wieczoru zasnąłem na piasku, o tysiąc mil od terenów zamieszkałych. Byłem bardziej
osamotniony, niŜ rozbitek na tratwie pośrodku oceanu. ToteŜ proszę sobie wyobrazić moje zdziwienie, gdy o
świcie obudził mnie czyjś głosik. Posłyszałem:
- Proszę cię, narysuj mi baranka.
- Co takiego?
- Narysuj mi baranka.

III
DuŜo czasu upłynęło, zanim zrozumiałem, skąd przybył. Mały KsiąŜę, choć sam zadawał mi wiele pytań
udawał, Ŝe moich nie słyszy dopiero z wypowiedzianych przypadkowo słów poznałem jego historię. Kiedy po
raz pierwszy ujrzał mój samolot (nie narysuję mego samolotu, poniewaŜ jest to rysunek zbyt trudny),
zapytał:

IV
W ten sposób dowiedziałem się drugiej waŜnej rzeczy: Ŝe planeta, z której pochodził, niewiele była większa
od zwykłego domu. Ta wiadomość nie zdziwiła mnie. Wiedziałem dobrze, Ŝe oprócz duŜych planet, takich
jak Ziemia, Jowisz, Mars, Wenus, którym nadano imiona, są setki innych, tak małych, Ŝe z wielkim trudem
moŜna je zobaczyć przy pomocy teleskopu. Kiedy astronom odkrywa którąś z nich, daje jej zamiast imienia
numer. Nazywa ją na przykład gwiazdą 3251.

V
Codziennie dowiadywałem się czegoś nowego o planecie, o wyjeździe, o podróŜy. Wiadomości te gromadziły
się z wolna i przypadkowo. I tak trzeciego dnia poznałem dramat baobabów.
W tym przypadku stało się to dzięki barankowi. Mały KsiąŜę spytał mnie nagle, jakby w coś zwątpił:

VI
Powoli zrozumiałem, Mały KsiąŜę, twoje smutne Ŝycie. Od dawna jedyną rozrywką był dla ciebie urok
zachodów słońca. Ten nowy szczegół twego Ŝycia poznałem czwartego dnia rano, gdyś mi powiedział:
- Bardzo lubię zachody słońca. Chodźmy zobaczyć zachód słońca.
- Ale trzeba poczekać.
- Poczekać na co?
- Poczekać, aŜ słońce zacznie zachodzić.
Początkowo zrobiłeś zdziwioną minę, a później roześmiałeś się i rzekłeś:
- Ciągle mi się wydaje, Ŝe jestem u siebie.
Rzeczywiście, wszyscy wiedzą, Ŝe kiedy w Stanach Zjednoczonych jest godzina dwunasta, we Francji słońce
zachodzi. Gdyby moŜna się było w ciągu minuty przenieść ze Stanów do Francji,oglądałoby się zachód
słońca. Niestety, Francja jest daleko. Ale na twojej planecie mogłeś przesunąć krzesełko o parę kroków i
oglądać zachód słońca tyle razy, ile chciałeś ...
- Pewnego dnia oglądałem zachód słońca czterdzieści trzy razy - powiedział Mały KsiąŜę, a w chwilę później
dodał:
- Wiesz, gdy jest bardzo smutno, to kocha się zachody słońca.
- Więc wówczas gdy oglądałeś je czterdzieści trzy razy, byłeś aŜ tak bardzo smutny? - zapytałem.
Ale Mały KsiąŜę nie odpowiedział. 

VII
Piątego dnia, znowu dzięki barankowi, odkryłem nową tajemnicę Małego Księcia. Gwałtownie, bez Ŝadnego
wstępu, zapytał mnie, jak gdyby po długim zastanawianiu się w samotności:
- JeŜeli baranek zjada krzaki, to je takŜe kwiaty?
- Baranek je wszystko, co napotka.
- Nawet kwiaty, które mają kolce?
- Tak, nawet kwiaty, które mają kolce.
- A więc do czego słuŜą kolce?

VIII
Wkrótce poznałem lepiej ten kwiat. Na planecie Małego Księcia kwiaty były zawsze bardzo skromne, o
pojedynczej koronie płatków, nie zajmujące miejsca i nie przeszkadzające nikomu. Pojawiały się któregoś
ranka wśród traw i więdły wieczorem. Krzak róŜy wykiełkował w ciągu dnia z ziarna przyniesionego nie
wiadomo skąd i Mały KsiąŜę z uwagą śledził ten pęd, zupełnie niepodobny do innych pędów.Mógł to być
nowy gatunek baobabu. Lecz krzak szybko przestał rosnąć i zaczął się formować kwiat. Mały KsiąŜę, który

IX
Sądzę, Ŝe dla swej ucieczki Mały KsiąŜę wykorzystał odlot wędrownych ptaków. Rano przed odjazdem
uporządkował dokładnie planetę. Pieczołowicie przeczyścił wszystkie wulkany. Miał dwa czynne wulkany. To
się bardzo przydaje do podgrzewania śniadań. Miał teŜ jeden wulkan wygasły. PoniewaŜ powtarzał zwykle: -
Nic nigdy nie wiadomo - więc przeczyścił takŜe wygasły wulkan. Jeśli wulkany są dobrze przeczyszczone,
palą się powoli i równo, bez wybuchów. Wybuchy wulkanów są tym, czym zapalenie sadzy w kominie.

X (...)
XI (...)
(...)
 
